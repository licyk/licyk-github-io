---
title: 工具合集
date: 2023-07-13 11:59:18
---
这里收集了一些工具和资源，可供使用

****
|个人网盘||
|---|---
|[蓝奏云(密码1234)](https://wwx.lanzoui.com/b01nl284f)|[123云盘](https://www.123pan.com/s/wO1SVv-rxW5v)


|教程(比较杂)||
|---|---
|[电脑使用经验](/2023/07/13/pc-use-experience)|[Windows激活](/2023/07/13/windows-activate)
|[Windows设置优化](/2023/07/13/windows-optimize)|[Cinnamon设置优化](/2023/07/13/cinnamon-optimize)
|[Linux命令教程](https://wangchujiang.com/linux-command/)|[Stable Diffusion教程](/2023/08/01/stable-diffusion-tutorial)


|个人软件存档列表||
|---|---
|[Windows](/2023/07/14/windows-software)
|[Android](/2023/07/14/android-software)
|[Linux](/2023/07/15/linux-software)
|[AI](https://licyk.netlify.app/2023/11/04/ai-tools-collection)
|[个人制作项目](/2023/07/18/personal-software-download-page)


|网页工具||
|---|---|
|[QQ消息撤回名片生成](https://licyk.github.io/t/q)|[仿浏览器页面](https://licyk.github.io/t/b)
|[滑稽](https://licyk.github.io/t/h)|[网页工具合集](/2023/07/17/web-tools)


|应用商店||
|---|---
|[百度手机助手](https://mobile.baidu.com/)|[360手机助手](http://m.app.haosou.com/)
|[应用宝](https://cftweb.3g.qq.com/qqappstore/index)|[应用汇](http://m.appchina.com/)
|[酷安](https://www.coolapk.com/apk/)|[豌豆荚](https://m.wandoujia.com/)
|[PP助手](https://wap.pp.cn/)|[Apkpure](http://m.apkpure.com/cn)
|[小米应用商城](https://app.mi.com/)|[华为应用市场](https://appgallery.huawei.com)
|[F-Droid](https://f-droid.org/)|[Malavida](https://www.malavida.com/en/android/)
|[uptodown](https://cn.uptodown.com/android)|[APK Support](https://apk.support/zh_cn/)
|[腾讯软件中心(PC)](https://pc.qq.com/)|[360软件管家(PC)](https://soft.360.cn/)


|机场推荐|[XYZ](https://9.234456.xyz/abc.html?t=567)|
|---|---


|我的世界||
|---|---
|[导航站](https://www.mcnav.net/)|[版本库导航](https://zihao-il.github.io/)
|[安卓/WIN版本库(原版)](https://mcarc.gitee.io/)|[安卓版本库(去验证)](https://mc233.endyun.ltd/)
|[安卓/IOS/WIN版本库(去验证)](https://mc.minebbs.com)|[安卓版本库(去验证)](http://mcapks.net/)
|[安卓/WIN版本库(去验证)](https://mcbbk.blmcpia.com)|[WIN版本库(原版)](https://www.mcappx.com/)


|专业工具下载||
|---|---
|[Vposy](https://mp.weixin.qq.com/mp/homepage?__biz=MzIyNjU2NzIxNQ==&hid=2&sn=0d0cb7f7ef080cb1fb6672e01ee632eb)|微信公众号搜索“Vposy”，关注后回复“软件目录”


|工具下载||
|---|---
|[异星软件空间](https://www.yxssp.com/)|[果核剥壳](https://www.ghxi.com/)
|[爱纯净](http://www.aichunjing.com/)|


|Windows镜像下载||
|---|---
|[Itellyou](https://msdn.itellyou.cn/)|[Next.Itellyou](https://next.itellyou.cn/)
|[系统库](https://www.xitongku.com/)|[HelloWindows](https://hellowindows.cn/)
|[山己几子木](https://msdn.sjjzm.com/)|[]()
|[官方Windows10](https://www.microsoft.com/zh-cn/software-download/windows10)|[官方Windows11](https://www.microsoft.com/zh-cn/software-download/windows11)


|Windows11绕限制直升教程||
|---|---
|[教程](https://www.bilibili.com/video/BV1jG4y1x7z1/)|有效


|镜像站||||
|---|---|---|---
|镜像站合集|[校园网联合镜像站](https://mirrors.cernet.edu.cn/)
|flatpak镜像站|[SJTU](https://mirror.sjtu.edu.cn/docs/flathub)
|pip镜像站|[SJTU](https://mirror.sjtu.edu.cn/docs/pypi/web/simple)|[BFSU](https://mirrors.bfsu.edu.cn/help/pypi/)|[TUNA](https://mirrors.tuna.tsinghua.edu.cn/help/pypi/)
|pytorch镜像站|[SJTU](https://mirror.sjtu.edu.cn/pytorch-wheels)|[aliyun](https://developer.aliyun.com/mirror/pytorch-wheels)

