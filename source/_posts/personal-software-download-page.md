---
title: 个人制作项目
date: 2023-07-18 09:50:57
tags:
categories:
 - 其他
---
这里展示我制作的小项目的下载页
&nbsp;
<!-- more -->
***

* # Term-SD
一款基于dialog实现界面显示的管理器，支持安装，管理A1111-SD-Webui,ComfyUI.InvokeAI,lora-scripts
下载前请先看[`使用说明`](https://github.com/licyk/sd-webui-script/blob/main/README.md)

||下载地址|
|---|---
|↓|[`源码`](https://github.com/licyk/sd-webui-script)
|↓|[`下载地址1`](https://ghproxy.com/https://github.com/licyk/term-sd/releases/download/v0.6.0/term-sd.sh)
|↓|[`下载地址2`](https://github.com/licyk/term-sd/releases/download/v0.6.0/term-sd.sh)

***

* # Scrcpy-TUI
一款基于终端显示的图形化scrcpy工具
下载前请看[`使用说明`](https://github.com/licyk/scrcpy-tui/blob/main/README.md)

||下载地址|
|---|---
|↓|[`源码`](https://github.com/licyk/scrcpy-tui)
|↓|<a href="https://ghproxy.com/https://raw.githubusercontent.com/licyk/scrcpy-tui/main/scrcpy-tui.sh?response-content-type=application%2Foctet-stream" download>`下载地址1`</a>
|↓|<a href="https://raw.githubusercontent.com/licyk/scrcpy-tui/main/scrcpy-tui.sh?response-content-type=application%2Foctet-stream" download>`下载地址2`</a>

***

* # bili-dl-merge
一个音频和视频合并脚本，用于哔哩哔哩安卓端的缓存视频
下载前请看[`使用说明`](https://github.com/licyk/bili-dl-merge/blob/main/README.md)

||下载地址|
|---|---
|↓|[`源码`](https://github.com/licyk/bili-dl-merge)
|↓|<a href="https://ghproxy.com/https://raw.githubusercontent.com/licyk/bili-dl-merge/main/bili-dl-merge.sh?response-content-type=application%2Foctet-stream" download>`下载地址1`</a>
|↓|<a href="https://raw.githubusercontent.com/licyk/bili-dl-merge/main/bili-dl-merge.sh?response-content-type=application%2Foctet-stream" download>`下载地址2`</a>

***

# ani2xcur
一个将windows鼠标指针转换为linux鼠标指针的脚本，拥有TUI界面，转换核心基于win2xcur
下载前请看[`使用说明`](https://github.com/licyk/ani2xcur/README.md)

||下载地址|
|---|---
|↓|[`源码`](https://github.com/licyk/ani2xcur)
|↓|<a href="https://ghproxy.com/https://raw.githubusercontent.com/licyk/ani2xcur/main/ani2xcur.sh" download>`下载地址1`</a>
|↓|<a href="https://raw.githubusercontent.com/licyk/ani2xcur/main/ani2xcur.sh" download>`下载地址2`</a>

***

# ani2xcur-core
这个是ani2xcur的核心版本，去掉了dialog,只保留基本的转换功能
下载前请看[`使用说明`](https://github.com/licyk/ani2xcur/README.md)

||下载地址|
|---|---
|↓|[`源码`](https://github.com/licyk/ani2xcur)
|↓|<a href="https://ghproxy.com/https://raw.githubusercontent.com/licyk/ani2xcur/main/ani2xcur-core.sh" download>`下载地址1`</a>
|↓|<a href="https://raw.githubusercontent.com/licyk/ani2xcur/main/ani2xcur-core.sh" download>`下载地址2`</a>

***