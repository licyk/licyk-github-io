---
title: AI软件合集
date: 2023-11-4 16:51:22
tags:
 - linux
 - windows
categories:
 - 软件
---
ai软件收集
<!-- more -->

|||
|---|---
|[DiffSinger](https://github.com/openvpi/DiffSinger)|
|[AntiFraudChatBot](https://github.com/Turing-Project/AntiFraudChatBot)|
|[Open-Assistant](https://github.com/LAION-AI/Open-Assistant)|
|[ultimatevocalremovergui](https://github.com/Anjok07/ultimatevocalremovergui)|
|[MoeVoiceStudio](https://github.com/NaruseMioShirakana/MoeVoiceStudio)|
|[stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)|
|[ColossalAI](https://github.com/hpcaitech/ColossalAI)|
|[ComfyUI](https://github.com/comfyanonymous/ComfyUI)|
|[lora-scripts](https://github.com/Akegarasu/lora-scripts)|
|[OpenChatKit](https://github.com/togethercomputer/OpenChatKit)|
|[InvokeAI](https://github.com/invoke-ai/InvokeAI)|
|[Grounded-Segment-Anything](https://github.com/IDEA-Research/Grounded-Segment-Anything)|
|[segment-anything](https://github.com/facebookresearch/segment-anything)|
|[SD.NEXT](https://github.com/vladmandic/automatic)|
|[whisper](https://github.com/openai/whisper)|
|[localrf](https://github.com/facebookresearch/localrf)|
|[Retrieval-based-Voice-Conversion-WebUI](https://github.com/RVC-Project/Retrieval-based-Voice-Conversion-WebUI)|
|[diffusers](https://github.com/huggingface/diffusers)|
|[text-generation-webui](https://github.com/oobabooga/text-generation-webui)|
|[llama.cpp](https://github.com/ggerganov/llama.cpp)|
|[Qwen](https://github.com/QwenLM/Qwen)|
|[Llama2-Chinese](https://github.com/FlagAlpha/Llama2-Chinese)|
|[kohya_ss](https://github.com/bmaltais/kohya_ss)|
|[GhostReview](https://github.com/drnighthan/GhostReview)|
|[Fooocus](https://github.com/lllyasviel/Fooocus)|
|[CodeGeeX2](https://github.com/THUDM/CodeGeeX2)|
|[nougat](https://github.com/facebookresearch/nougat)|
|[mlc-llm](https://github.com/mlc-ai/mlc-llm)|
|[stable-diffusion-webui-directml](https://github.com/lshqqytiger/stable-diffusion-webui-directml)|
|[anime-segmentatio](https://github.com/SkyTNT/anime-segmentation)|
|[ChatGLM3](https://github.com/THUDM/ChatGLM3)|
|[transformers](https://github.com/huggingface/transformers)|
|[opencv](https://github.com/opencv/opencv)|
|[ultralytics](https://github.com/ultralytics/ultralytics)|
