---
title: 资料合集
date: 2023-11-4 16:56:22
tags:
categories:
 - 其他
---
收集github上一些自学资料
<!-- more -->

|||
|---|---
|[How-To-Ask-Questions-The-Smart-Way](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way)|
|[Stop-Ask-Questions-The-Stupid-Ways](https://github.com/tangx/Stop-Ask-Questions-The-Stupid-Ways)|
|[README](https://github.com/guodongxiaren/README)|
|[REKCARC-TSC-UHT](https://github.com/PKUanonym/REKCARC-TSC-UHT)|
|[ello-algo](https://github.com/krahets/hello-algo)|
|[cs-self-learning](https://github.com/PKUFlyingPig/cs-self-learning)|
|[GitHubDaily](https://github.com/GitHubDaily/GitHubDaily)|
|[awesome-programming-books](https://github.com/jobbole/awesome-programming-books)|
|[annotated_deep_learning_paper_implementations](https://github.com/labmlai/annotated_deep_learning_paper_implementations)|
|[rust-course](https://github.com/sunface/rust-course)|
|[linux-command](https://github.com/jaywcjlove/linux-command)|
|[manpages-zh](https://github.com/man-pages-zh/manpages-zh)|
|[StableDiffusionBook](https://github.com/sudoskys/StableDiffusionBook)|