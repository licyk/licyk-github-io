---
title: stable diffusion教程
date: 2023-08-01 17:09:18
tags:
categories:
 - 教程
---
整理了stable diffusion的教程
<!-- more -->
***
&nbsp;
# Stable Diffusion Webui

## 系统教程

>推荐观看该系列视频学习

|系统的AI绘画教程 by bilibili@[Nenly同学](https://space.bilibili.com/1814756990) ||
|---|---|
|正式课||
|[先导课：了解AI绘画的发展现状](https://www.bilibili.com/video/BV16u411j7u3)|
|[第一课：20分钟，光速入门AI绘画!原理解析+配置要求+应用安装+基本步骤](https://www.bilibili.com/video/BV1As4y127HW)|
|[第二课：史上最细的AI绘画提示词与参数设置攻略](https://www.bilibili.com/video/BV12X4y1r7QB)|
|[第三课：打破次元壁！AI图生图“重绘”照片和CG](https://www.bilibili.com/video/BV1Lh411u7vs)|
|[第四课：AI绘画模型新手包！“画风”自由切换，有哪些你不知道的模型使用技巧？](https://www.bilibili.com/video/BV1Us4y117Rg)|
|[第五课：拒绝低质量出图！3种快速提高AI绘画分辨率的方式，十分钟讲完！](https://www.bilibili.com/video/BV11m4y12727)|
|[第六课：Embeddings , LoRa , Hypernetwork概念简析](https://www.bilibili.com/video/BV1th4y1p7fH)|
|[第七课：AI绘画的救星！定向修手修脸，手把手教你玩转局部重绘！](https://www.bilibili.com/video/BV1uL411e7Uk)|
|[第八课：提示词补全翻译反推，“终极”放大脚本与细节优化插件，这些AI绘画扩展真的泰厉害辣！](https://www.bilibili.com/video/BV1hz4y1a76M)|
|[第九课：LoRA从原理到实践，零基础打造赛博Coser、一键更换服饰，无所不能！5大应用方向剖析 & 保姆级讲解！](https://www.bilibili.com/video/BV1nL411B7XT)|
|[第十课：30分钟零基础掌握ControlNet！绝对是你看过最好懂的控制网原理分析  基本操作、插件安装与5大模型应用](https://www.bilibili.com/video/BV1Ds4y1e7ZB)|
|加餐课||
|[扩展应用：Multi Diffusion + Tiled VAE + ControlNet Tile模型，低显存打造AI绘画超高清6K分辨率体验！SD扩展插件教程](https://www.bilibili.com/video/BV1Su4y1d7Dp)|
|[扩展应用：“牛逼”的教程来了！一次学会AI二维码+艺术字+光影光效+创意Logo生成](https://www.bilibili.com/video/BV1gX4y1J7ei)|
|[扩展应用：15分钟入门AI动画！Mov2Mov零基础教学，用Stable Diffusion生成酷炫逐帧重绘动画短视频](https://www.bilibili.com/video/BV1Su411b7Nm)|
|[扩展应用：10倍效率，打造无闪烁丝滑AI动画！EbSynth插件全流程操作解析与原理分析，超智能的“补帧”动画制作揭秘](https://www.bilibili.com/video/BV1uX4y1H7U3)|
|[模型推荐：AI再进化，这次竟然学会摄影了！一秒生成胶片风、拍立得，还能智能“修脸”！Stable Diffusion AI绘画真实系人像模型+Lora分享](https://www.bilibili.com/video/BV1DP41167bZ)|
|[模型推荐：一流模型！10个高手都在用的二次元插画绘画风大模型推荐，内附模型下载渠道与出图参数提示词关键词模版](https://www.bilibili.com/video/BV1Us4y1M7x4)|

&nbsp;

>这个系统教程虽然出的比较早，但是可作为补充学习Nenly同学未提到的东西

|轩轩的sd webui教程 by bilibili@[靠谱的轩轩](https://space.bilibili.com/1642297)||
|---|---
|[【基础01】新人必看！stable diiffusion大模型checkpoint放哪里？怎么加载?在哪里下？](https://www.bilibili.com/video/BV1mv4y187qk)|
|[【基础02】stable diffusion的VAE是啥？有啥用？怎么用？有啥效果？](https://www.bilibili.com/video/BV1LY411z7Wy)|
|[【基础03】stable diffusion的embedding是个啥？有啥用？怎么用？有啥效果？](https://www.bilibili.com/video/BV1Pg4y1t74E)|
|[【基础04】目前全网最贴心的Lora基础知识教程！stable diffusion新手福音，功能，下载地址，安装使用方法全知道（上篇）](https://www.bilibili.com/video/BV1ML411d7pR)|
|[【基础05】使用lora后效果不好？全网最贴心的Lora新手使用技巧教程](https://www.bilibili.com/video/BV1mL41197UM)|
|[【基础06】超贴心的hypernetwork基础知识教程！](https://www.bilibili.com/video/BV1ZL411Q7cy)|
|[【基础07】5秒钟分清模型类型，5分钟搞懂尾缀和模型关系。](https://www.bilibili.com/video/BV1aa4y1M7KZ)|
|[AI绘画软件比较与stable diffusion的优势](https://www.bilibili.com/video/BV1hc411W7Av)|
|[stable diffusion应用场景略讲](https://www.bilibili.com/video/BV1aa4y1K7bc)|
|[AI绘画用户画像](https://www.bilibili.com/video/BV1SN411A7uD)|
|[新手要理解的stable diffusion的一点点原理](https://www.bilibili.com/video/BV1TM4y1m7Nv)|
|[大模型-vae-clip跳过层讲解](https://www.bilibili.com/video/BV1Jh411g7aL)|
|[提示词功能-提示词超简单写作思路](https://www.bilibili.com/video/BV1nh411g7NN)|
|[提示词8类语法精讲](https://www.bilibili.com/video/BV1ps4y1m7od)|
|[采样迭代步数-采样方法讲解](https://www.bilibili.com/video/BV1AM4y117td)|
|[面部修复-高清修复讲解与技巧](https://www.bilibili.com/video/BV1r24y157h7)|
|[图片高度与宽度的注意点](https://www.bilibili.com/video/BV1UL41127iJ)|
|[生成批次-每批数量-CFG讲解](https://www.bilibili.com/video/BV1H24y1w7nx)|
|[随机种子-脚本XYZ图表讲解](https://www.bilibili.com/video/BV1EX4y1r7Pf)|
|[脚本提示词矩阵-脚本批量载入讲解](https://www.bilibili.com/video/BV1Bm4y1B7AA)|
|[重绘幅度denoise讲解](https://www.bilibili.com/video/BV1Dk4y1Y7cu)|
|[图生图提示词-图片输入](https://www.bilibili.com/video/BV1qv4y1H7bk)|
|[缩放模式-图片传送](https://www.bilibili.com/video/BV1CL41127SS)|
|[图生图绘图模式](https://www.bilibili.com/video/BV1Ys4y1K798)|
|[局部重绘设计原理-蒙版模糊](https://www.bilibili.com/video/BV1Ws4y117nh)|
|[蒙版模式-蒙版蒙住内容](https://www.bilibili.com/video/BV1mk4y1Y7qj)|
|[重绘区域-全图](https://www.bilibili.com/video/BV12m4y1B7QJ)|
|[蒙版模式-仅蒙版-边缘预留像素](https://www.bilibili.com/video/BV1ih411M7Sn)|
|[局部重绘（手涂蒙版）](https://www.bilibili.com/video/BV1um4y1m7S4)|
|[蒙版模式(上传蒙版)-批量处理](https://www.bilibili.com/video/BV1n24y1w7Xy)|
|[图片信息反推-Tag反推](https://www.bilibili.com/video/BV1NX4y1r7zX)|
|[图片高清放大-人脸修复](https://www.bilibili.com/video/BV1HX4y1r7x4)|
|[controlnet设计思路与设置修改](https://www.bilibili.com/video/BV1Vo4y187Bc)|
|[预处理器与controlnet模型](https://www.bilibili.com/video/BV1La4y1N7VZ)|
|[Controlnet线条约束](https://www.bilibili.com/video/BV1Ko4y1H7Po)|
|[Controlnet姿势约束与深度约束](https://www.bilibili.com/video/BV1vX4y1r7Eb)|
|[语义分割-色彩约束-风格约束](https://www.bilibili.com/video/BV11s4y117Sc)|
|[Controlnet剩余参数讲解](https://www.bilibili.com/video/BV19M4y1C72v)|
|[【习题01】如何定制AI模特](https://www.bilibili.com/video/BV1654y1F7SE)|
|[【习题02】如何生成高清模特大图](https://www.bilibili.com/video/BV1xg4y1u7Ad)|
|[【习题03】如何比较不同模型效果](https://www.bilibili.com/video/BV1ea4y1N7QW)|
|[【习题04】如何比较不同衣服提示词的效果？](https://www.bilibili.com/video/BV1XM4y1C77L)|
|[【习题05】图片如何风格转换](https://www.bilibili.com/video/BV1ks4y1171q)|
|[【习题06】产品、人像如何高清修复](https://www.bilibili.com/video/BV1ZL411f7x9)|
|[【习题07】如何把线稿上色](https://www.bilibili.com/video/BV1Ws4y1P7Z6)|
|[【习题08】根据线稿生成室内设计](https://www.bilibili.com/video/BV17s4y1P7xo)|
|[【习题9】如何转换室内装修风格与地板](https://www.bilibili.com/video/BV1yN411w7dX)|

&nbsp;

## 其他应用


|[秋葉aaaki](https://space.bilibili.com/12566101)的教程||
|---|---
|[【AI绘画】从零开始的AI绘画入门教程——魔法导论](https://www.bilibili.com/read/cv22159609)|
|[Dreambooth模型介绍和模型融合教程](https://www.bilibili.com/video/BV1Ae4y1s79r)||
|[Dreambooth训练教程](https://www.bilibili.com/video/BV1SR4y1y7Lv)|
|[LoRA模型训练教程](https://www.bilibili.com/video/BV1fs4y1x7p2)|
|[Embedding 训练教程](https://www.bilibili.com/video/BV1C14y157nQ/)|
|[ControlNet1.1 使用教程](https://www.bilibili.com/video/BV1fa4y1G71W)|
|[深入理解Stable Diffusion!从原理到模型训练](https://www.bilibili.com/video/BV1x8411m76H)

&nbsp;

>学习训练Lora的推荐该系列教程

|Lora训练 by bilibili@[青龙圣者](https://space.bilibili.com/219296)||
|---|---|
|[【AI绘画设置优化】LORA训练加速100%速度](https://www.bilibili.com/video/BV1Ts4y1J76y)|
|[【新算法优化】最新LoRA模型训练进阶教程1](https://www.bilibili.com/video/BV1pA411m7WA)|
|[【快速打标/优化器对比】最新LoRA模型训练进阶教程2](https://www.bilibili.com/video/BV15j411F7nJ)|
|[【正则化和卷积应用】最新LoRA模型训练进阶教程3](https://www.bilibili.com/video/BV1MM41177xv)|
|[【正则化制作功能LoRA】最新LoRA模型训练进阶教程4](https://www.bilibili.com/video/BV1oP411f7fi)|
|[【哈达玛积训练画风LoRA】最新LORA模型训练教程5](https://www.bilibili.com/video/BV1HM4y167Hg)|
|[【LoRA训练用什么底模】最新LoRA训练进阶教程6](https://www.bilibili.com/video/BV1hk4y1Y7sc)|
|[【动态lora！任意dim值选择】最新LoRA训练进阶教程7](https://www.bilibili.com/video/BV1Qc411n7V8)|
|[【金字塔噪声，接近MJ的效果】最新LoRA训练进阶教程8](https://www.bilibili.com/video/BV12P411y76P)|
|[【lora分层的底层原理】最新LoRA训练进阶教程9](https://www.bilibili.com/video/BV1th411F7CR)|
|[【完美炼丹术，差异炼丹法】最新LoRA训练进阶教程10](https://www.bilibili.com/video/BV11m4y147WQ)|
|[【神童优化器！全程自适应训练】最新lora训练教程11](https://www.bilibili.com/video/BV1nX4y1H7x5)|
|[【0图流正负丹，减少手部崩坏】最新Lora训练AI高级教程12](https://www.bilibili.com/video/BV1dm4y1L7q3)|
|[【SDXL8G优化训练，脚本更新】最新Lora训练AI高级教程13](https://www.bilibili.com/video/BV1tk4y137fo)|

&nbsp;

|其他lora训练教程||
|---|---|
|[lora训练进阶教程，6分半教会你怎么训练一个好的lora](https://www.bilibili.com/video/BV1jP411f7kV)|
|[赛博德狗lora作者，c站前10下载量作者的lora训练心得](https://www.bilibili.com/video/BV1Nc411W7PG)|
|[【AI绘画】较详细！正则化！从泛化性角度谈Lora模型训练心得](https://www.bilibili.com/video/BV1GM411E7vk)|
|[【LORA训练】10分钟教你训练画风lora！](https://www.bilibili.com/video/BV15j411c7dL)|
|[LoRA 角色模型训练-训练脚本作者亲自指导](https://www.bilibili.com/video/BV1Js4y1c7oU)|
|[【AI绘画】训练LoRA模型最重要的一环：打标签](https://www.bilibili.com/video/BV1ZM411c7aj)|
|[【AI绘画】Lora制作新时代！从大模型提取Lora , Lora合并 使用教程](https://www.bilibili.com/video/BV1t24y1J7Bk)|
|[【AI绘画】超级模型融合插件详解，Lora提取，MBW大模型合并，Lora融合大模型，xy对比融合图](https://www.bilibili.com/video/BV1J84y1E7nn/)|
|[如何快速训练番剧画风lora](https://www.bilibili.com/video/BV1Rs4y1w76w/)|

&nbsp;

|dreambooth训练 by bilibili@[小李xiaolxl](https://space.bilibili.com/34590220)||
|---|---
|[DreamBooth画风训练教程(基于NovelAI)](https://www.bilibili.com/video/BV18K411o7mG/)|
|[DreamBooth人物训练与安装教程](https://www.bilibili.com/video/BV1g841187nc/)|
|[Lora+OffsetNoise+Lion+locon全解(基于DreamBooth可视化训练插件)](https://www.bilibili.com/video/BV1sM4y1C7YZ/)|

&nbsp;

|其他使用教程||
|---|---
|[超详细的局部重绘讲解](https://www.bilibili.com/video/BV1HN411N7GB)|
|[stable diffusion AI绘画入门19 思维决定画面效果](https://www.bilibili.com/video/BV1X14y1o76e)|
|[为什么你的AI绘画图片质量这么低？20分钟学会全方位提高画质的方法](https://www.bilibili.com/video/BV1Qk4y1i726)|
|[【AI绘画】webUI中各种放大算法效果对比与原理简介](https://www.bilibili.com/video/BV13M4y1d7TU)|
|[【AI绘画】openpose新插件poseX，360°3D旋转~秒杀editor，controlnet好助手](https://www.bilibili.com/video/BV13j411u7SZ)|
|[【AI绘画】手势生成再进化？OpenPose-HAND对比Depth Library插件 全流程教学](https://www.bilibili.com/video/BV1ML41127fu/)|
|[【主打开源】stable diffusion+blender 我今天就要画手](https://www.bilibili.com/video/BV1Ec41157Hp/)|
|[【AI绘画】利用LoRA分层控制高效且有目的地融合画风](https://www.bilibili.com/video/BV1ms4y1E7zX/)|
|[【AI绘画】在画面不同区域使用不同关键词！潜变量成对（latent couple）参数怎么填](https://www.bilibili.com/video/BV1NL411C7Ja/)|
|[Latent Couple分区渲染详解+Lora防污染手段](https://www.bilibili.com/video/BV12h411G7T5/)|
|[【AI绘画教程】B站最通俗易懂的loRA分层控制与融合教程⚡️5分钟解决污染](https://www.bilibili.com/video/BV1nP411U7me/)|
|[Stable Diffusion Latent Couple、Composable LoRA完成多人](https://www.bilibili.com/video/BV1zL41127Sg)|
|[AI绘画】Stable Diffusion Regional Prompter插件教学，实现多区域绘画自由](https://www.bilibili.com/video/BV1Jz4y1b7dJ)|
|[【Ai绘画】VRM+ControlNET文生图+实景照片，控制最终画面](https://www.bilibili.com/video/BV1Rc411H7F4)|
|[【AI绘画】利用ControlNet和线稿LoRA提取线稿的方法+一些新用法](https://www.bilibili.com/video/BV1Nv4y1a7ts/)|
|[【AI绘画】使用ControlNet给线稿上色的技巧](https://www.bilibili.com/video/BV1pv4y18793)|
|[【AI绘画】ControlNet Tile 工作流](https://www.bilibili.com/video/BV19m4y187o1/)|
|[【AI绘画】利用ControlNet清理草稿](https://www.bilibili.com/video/BV1t54y1M76a/)|
|[stable-diffusion动作+背景【全控】-controlnet seg+动作+深度图的用法](https://www.bilibili.com/video/BV1dP411Z7S8)|
|[【AI绘画进阶教程】图生图 让AI模特穿上你喜欢的衣服](https://www.bilibili.com/video/BV1uM411u739)|
|[【AI教学】草稿秒变成图？线稿快速上色？](https://www.bilibili.com/video/BV13o4y1z72w)|
|[【AI绘画进阶教程】seg 语义分割控制网络 ControNet教你画景填人](https://www.bilibili.com/video/BV1Wg4y1x7Ur/)|
|[【AI绘画】利用GroundingDINO+SAM快速修改图片局部的方法](https://www.bilibili.com/video/BV1Hh411j7b2)|
|[拯救崩图！提高CFG影响的同时保持画面stable diffusion webui实用插件推荐](https://www.bilibili.com/video/BV1tM411W7Ea/)|
|[用PS控制Stablediffusion，听话的AI才是好AI](https://www.bilibili.com/video/BV1wh4y1G7kR/)|
|[Stable Diffusion fp16(半精) vs fp32(单精), Pruned model vs Full mod,出图速度对比，图片质量对比](https://www.bilibili.com/video/BV1dX4y1e7Yr/)|
|[AI绘画 多LoRA模型的使用与管理教程](https://www.bilibili.com/video/BV1j24y1g749)|
|[StableDiffusion AI绘画工作流进阶教程-第五课：超采再放大](https://www.bilibili.com/video/BV1c14y1U7UF)|
|[【AI绘画】如何绘制特殊比例超大图片](https://www.bilibili.com/video/BV1ij411c7iS/)|
|[基于LyCoRIS的LoRA模型：LoHa](https://www.bilibili.com/video/BV1cj411A73a)
|[Lycoris画风模型权重优化](https://www.bilibili.com/video/BV18c411J7Ms)

&nbsp;

|stable diffusion原理 by bilibili@[FDX01](https://space.bilibili.com/23706358)||
|---|---
|[浅谈stable diffusion (一)](https://www.bilibili.com/read/cv22266484)
|[浅谈stable diffusion (二)](https://www.bilibili.com/read/cv22533819)
|[浅谈stable diffusion (三)](https://www.bilibili.com/read/cv22642045)
|[浅谈stable diffusion (四)](https://www.bilibili.com/read/cv22817096)

&nbsp;

|stable diffusion工具||
|---|---
|[3D Openpose Editor](https://zhuyu1997.github.io/open-pose-editor/)|
|[语义分割工具](https://huggingface.co/spaces/shi-labs/OneFormer)|
|[咒语生成器](https://www.wujieai.com/tag-generator)|
|[魔咒百科词典](https://aitag.top/)|
|[civitai](https://civitai.com/)|
|[huggingface](https://huggingface.co)|
|[esheep](https://www.esheep.com/)|

&nbsp;

***

&nbsp;

# ComfyUI

|comfyui教程 by bilibili@[只剩一瓶辣椒酱](https://space.bilibili.com/35723238)||
|---|---|
|[【专业向节点AI】SD ComfyUI大冒险-基础篇 00安装与部署](https://www.bilibili.com/video/BV1Fo4y187HC)|
|[【专业向节点AI】SD ComfyUI大冒险-基础篇 01万法之基 文生图](https://www.bilibili.com/video/BV1zh4y1p761)|
|[【专业向节点AI】SD ComfyUI大冒险-基础篇 02推演变体 图生图](https://www.bilibili.com/video/BV16h411j74E)||
|[【专业向节点AI】SD ComfyUI大冒险 -基础篇 03高清输出 放大奥义](https://www.bilibili.com/video/BV1Jk4y1J7GP)|
|[【专业向节点AI】SD ComfyUI大冒险-基础篇 04数据微调 Lora与超网络](https://www.bilibili.com/video/BV1uh411j7Vd)|
|[【ComfyUI中文版】Blender x ComfyUI节点式AI绘画大冒险-F1番外-通过读取图像获取节点树](https://www.bilibili.com/video/BV1Uo4y1x7vY)|
|[【专业向节点AI】SD ComfyUI大冒险-05ControlNET进阶-概述](https://www.bilibili.com/video/BV1yV4y1176S)|
|[【专业向节点AI】SD ComfyUI大冒险-06ControlNET进阶-线の控制](https://www.bilibili.com/video/BV1BX4y1s7wr)|
|[【专业向节点AI】SD ComfyUI大冒险-07ControlNET进阶-深度和法线控制](https://www.bilibili.com/video/BV1zV411u7R4)|
|[【专业向节点AI】SD ComfyUI大冒险--08ControlNET进阶-姿态和面部表情控制](https://www.bilibili.com/video/BV1wh411N7VU)|
|[【专业向节点AI】SD ComfyUI大冒险-09ControlNET进阶-语义分割控制](https://www.bilibili.com/video/BV13z4y1p7xt)|
|[【专业向节点AI】SD ComfyUI大冒险-10ControlNET进阶-颜色色彩控制](https://www.bilibili.com/video/BV1Eh4y1u7wA)|
|[【专业向节点AI】SD ComfyUI大冒险系统教程-11ControlNET进阶-Tile分块控制](https://www.bilibili.com/video/BV1DV411M7WL)|
|[【专业向节点AI已汉化】SD ComfyUI大冒险系统教程-12 ControlNET进阶-Inpaint内补绘制(局部重绘)&发型发色实例](https://www.bilibili.com/video/BV1hP411y7hg)|
|[【专业向节点AI】SD ComfyUI大冒险系统教程-13 ControlNET进阶-Shuffle内容重组](https://www.bilibili.com/video/BV1WV4y1q7mE)|
|[【专业向节点AI】SD ComfyUI大冒险系统教程-14 ControlNET进阶-IP2P以图生图](https://www.bilibili.com/video/BV17m4y1j7W5)|

&nbsp;

|comfyui教程 by bilibili@[独立研究员-星空](https://space.bilibili.com/250989068)||
|---|---
|[未来已至，基于节点图的工作流才是AI绘画的未来](https://www.bilibili.com/video/BV1rM4y1r7S5)|
|[AI绘画 为什么说Automatic1111 WebUI是工程师思维的产物?](https://www.bilibili.com/video/BV1ts4y1H7E1)|
|[AI绘画 ComfyUI 生成高分辨率和丰富细节图像 本质是提升潜在空间分辨率](https://www.bilibili.com/video/BV1cs4y1o7oE)|
|[AI绘画 ComfyUI 使用Latent Couple和多区域组合节点 生成双人合照](https://www.bilibili.com/video/BV1DL411Y7HF)|
|[AI绘图 深入解析 分区渲染 Latent Couple](https://www.bilibili.com/video/BV1Fs4y1G7r6)|
|[Stable Diffusion XL 架构解析](https://www.bilibili.com/video/BV1Qz4y177d6)|
|[ComfyUI Stable Diffusion XL 最新工作流解读](https://www.bilibili.com/video/BV1WX4y1E7xm)|

&nbsp;

|官方教程||
|---|---
|[基础流程例子](https://comfyanonymous.github.io/ComfyUI_examples/)|
|[视觉小说教程](https://comfyanonymous.github.io/ComfyUI_tutorial_vn/)|
|[视觉小说教程(中文版)](https://comfyui.jpanda.cn/)|

&nbsp;

|其他教程||
|---|---
|[Stable Diffusion ComfyUI 入门感受](https://zhuanlan.zhihu.com/p/620297462)|

&nbsp;

***

&nbsp;

# InvokeAI

|||
|---|---
|[官方文档](https://invoke-ai.github.io/InvokeAI/)||