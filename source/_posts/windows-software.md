---
title: Windows软件合集
date: 2023-07-14 20:14:48
tags:
 - windows
categories:
 - 软件
---
Windows软件存档
<!-- more -->
***
|浏览器||
|---|---
|[Firefox](https://www.mozilla.org/zh-CN/firefox/all/#product-desktop-release)|开源
|[Microsoft Edge](https://www.microsoft.com/zh-cn/edge/download?form=MA13FJ&ch)|windows自带
|[Chrome](https://www.google.cn/chrome/index.html)|书签同步404
|[Chromium](https://chromium.googlesource.com/chromium/)|开源
|[BookmarkHub](https://github.com/dudor/BookmarkHub)|跨浏览器书签同步插件
|[PixivBatchDownloader](https://github.com/xuejianxianzun/PixivBatchDownloader)|pixiv批量下载工具


|显卡驱动||
|---|---
|[NVIDIA](https://www.nvidia.cn/geforce/drivers/)|NVIDIA显卡驱动
|[AMD](https://www.amd.com/zh-hans/support)|AMD显卡驱动
|[INTEL](https://www.intel.cn/content/www/cn/zh/download-center/home.html)|INTEL显卡驱动


|生产||
|---|---
|[Krita](https://krita.org/zh/)|绘画
|[Live2d Cubism](https://www.live2d.com/)|live2d制作
|[Audacity](https://www.audacityteam.org/)|音频编辑
|[MuseScore](https://musescore.org/zh-hans)|音乐制谱工具
|[Onlyoffice](https://www.onlyoffice.com/zh/)|开源office工具
|[Libreoffice](https://zh-cn.libreoffice.org/)|开源office工具
|[WPS](https://platform.wps.cn/)|国内office工具，上手简单
|[Microsoft 365](https://www.microsoft.com/zh-cn/microsoft-365/)|microsoft自家office工具
|[Opentoonz](https://opentoonz.github.io/)|动画制作
|[Blender](https://www.blender.org/)|开源，免费的3d建模，渲染软件
|[希沃易+](https://e.seewo.com/)|希沃全家桶
|[OBS Studio](https://obsproject.com/)|视频录制，直播，可扩展性强
|[Obs-websocket](https://obsproject.com/forum/resources/obs-websocket-remote-control-obs-studio-using-websockets.466/)|obs远程控制插件
|[Islide](https://www.islide.cc/)|office素材获取
|[HandBrake](https://handbrake.fr/)|视频转换和压制
|[格式工厂](http://www.pcgeshi.com/index.html)|格式转换
|[Utools](https://u.tools/)|效率提高工具，可玩性高
|[Broadcast](https://www.nvidia.cn/geforce/broadcasting/broadcast-app/)|麦克风降噪
|[RTX Voice](https://www.nvidia.cn/geforce/guides/nvidia-rtx-voice-setup-guide/)|麦克风降噪
|[Adobe Photoshop](https://www.adobe.com/cn/products/photoshop.html)|图片编辑
|[Adobe Premiere Pro](https://www.adobe.com/cn/products/premiere.html)|视频编辑
|[Adobe After Effects](https://www.adobe.com/cn/products/aftereffects.html)|视频特效制作
|[Adobe Audition](https://www.adobe.com/cn/products/audition.html)|音频编辑
|[Adobe Dreamweaver](https://www.adobe.com/cn/products/dreamweaver.html)|网页设计
|[Autodesk Maya](https://www.autodesk.com.cn/products/maya/overview)|3d建模，渲染
|[Autodesk 3ds Max](https://www.autodesk.com.cn/products/3ds-max/overview)|3d建模，渲染
|[FL STUDIO](https://www.flstudiochina.com/)|音乐制作
|[Slack](https://slack.com/intl/zh-cn/downloads/windows)|办公协作
|[Thunderbird](https://www.thunderbird.net/zh-CN/)|邮件收发平台
|[GIMP](https://www.gimp.org/downloads/)|开源，免费的图片编辑软件
|[Kdenlive](https://kdenlive.org/zh/features-zh/)|开源，免费的视频编辑软件
|[Davinci Resolve](https://www.blackmagicdesign.com/cn/products/davinciresolve/)|视频编辑
|[思源笔记](https://b3log.org/siyuan/download.html)|强大的笔记编辑和管理软件
|[Grasscutter](https://github.com/Grasscutters/Grasscutter)|游戏后端服务器
|[moligeek](https://github.com/yourmoln/moligeek)|web集成工具
|[Cloudreve](https://github.com/cloudreve/Cloudreve)|支持多家云存储的云盘系统
|[lightspark](https://github.com/lightspark/lightspark)|开源的flash运行库


|开发||
|---|---
|[Visual Studio code](https://code.visualstudio.com/)|强大的代码编辑器
|[Visual Studio](https://visualstudio.microsoft.com/zh-hans/)|集成式开发环境
|[Jetbrains](https://www.jetbrains.com/)|集成式开发环境
|[Theme Studio](https://developer.huawei.com/consumer/cn/huawei-theme-studio)|华为主题编辑器
|[JAVA](https://www.oracle.com/java/technologies/downloads/#jdk17-windows)|java开发环境
|[Python](https://www.python.org/)|python开发环境
|[pip](https://pypi.org/project/pip/#files)|python包管理器
|[.NET](https://dotnet.microsoft.com/zh-cn/download/dotnet)|.NET开发环境
|[Docker](https://www.docker.com/products/docker-desktop/)|开源的应用容器引擎
|[git](https://gitforwindows.org/)|开源的版本管理工具
|[Sourcetree](https://www.sourcetreeapp.com/)|git图形化工具
|[Tortoisegit](https://tortoisegit.org/)|git图形化工具
|[Gitkraken](https://www.gitkraken.com/)|git图形化工具
|[SmartGit](https://www.syntevo.com/smartgit/)|git图形化工具
|[MSYS2](https://www.msys2.org/)|shell 命令行开发环境，移植了pacman作为包管理器
|[Cygwin/X](http://x.cygwin.com/)|shell 命令行开发环境
|[Win-Bash](https://win-bash.sourceforge.net/)|shell 命令行开发环境
|[Gradio](https://www.gradio.app/)|快速创建图形用户界面(GUI)的Python库
|[Pytorch](https://pytorch.org/)|开源的Python机器学习库
|[nvm](https://github.com/nvm-sh/nvm)|node.js版本管理器
|[microsoft visual c++](https://learn.microsoft.com/zh-CN/cpp/windows/latest-supported-vc-redist?view=msvc-170)|Windows系统的c++运行库


|串流||
|---|---
|[Audiorelay](https://audiorelay.net/)|音频串流
|[SoundWire](https://georgielabs.net/)|音频串流
|[WO Mic](https://wolicheng.com/womic/)|麦克风串流
|[Webcam](http://iriun.com/)|摄像头串流
|[DroidCam](https://www.dev47apps.com/)|摄像头和麦克风串流
|[Macast](https://github.com/xfangfang/Macast)|多平台视频串流工具


|远程桌面||
|---|---
|[Parsec](https://parsec.app/)|低延迟游戏串流
|[RayLink](https://www.raylink.live/)|多平台远程桌面工具
|[Rustdesk](https://rustdesk.com/zh/)|可自建的开源远程桌面工具
|[Todesk](https://www.todesk.com/)|多平台远程桌面工具
|[Spacedesk](https://www.spacedesk.net/)|电脑屏幕扩展
|[贝锐向日葵](https://sunlogin.oray.com/)|多平台远程桌面工具
|[Monect](https://www.monect.com/)|远程控制
|[Newdesk](http://www.helpercow.com/)|多平台远程桌面工具


|代理工具||
|---|---
|[V2rayN](https://github.com/2dust/v2rayN)|代理工具
|[Clash](https://github.com/Fndroid/clash_for_windows_pkg)|代理工具
|[网易UU加速器](https://uu.163.com/)|网络加速器
|[Watt Toolkit](https://steampp.net/)|代理工具


|内网穿透||
|---|---
|[Zerotier](https://www.zerotier.com/)|内网穿透工具
|[Tailscale](https://tailscale.com/)|内网穿透工具


|玩机||
|---|---
|[Driverpack](https://driverpack.io/zh-cn)|驱动安装和管理
|[Driverfix](https://driverfix.com/)|驱动安装和管理
|[图拉丁吧工具箱](http://www.tbtool.cn/)|玩机工具箱
|[云图工具箱](https://wintool.cc/)|玩机工具箱
|[卡硬工具箱](http://www.kbtool.cn/)|玩机工具箱
|[dism++](https://github.com/Chuyu-Team/Dism-Multi-language/releases)|强大的 Windows 系统优化工具
|[傲梅分区助手](https://www.disktool.cn/)|硬盘分区与管理
|[Etcher](https://www.balena.io/etcher/)|启动盘制作器
|[rEFInd](http://www.rodsbooks.com/refind/getting.html)|多系统引导工具
|[Diskgenius](https://www.diskgenius.cn/)|硬盘分区与管理，数据恢复
|[Firpe](https://firpe.cn/)|支持网络的pe
|[微PE工具箱](https://www.wepe.com.cn/download.html)|简洁，强大的pe
|[AIDA64](https://www.aida64.com/)|硬件信息查看
|[Reimageplus](http://www.reimageplus.com/)|系统文件修复
|[Ventoy](https://www.ventoy.net/cn/)|多系统启动盘制作工具
|[WearOS工具箱](https://www.wearosbox.com/)|手表安卓系统玩机工具
|[秋之盒](https://www.atmb.top/)|安卓系统玩机工具
|[白眼](https://epcdiy.org/)|性能测式
|[Display Driver Uninstaller](https://www.wagnardsoft.com/display-driver-uninstaller-DDU-)|显卡驱动清理工具
|[Fan Control](https://getfancontrol.com/)|电脑风扇控制工具
|[ohook](https://github.com/asdcorp/ohook)|开源的office激活工具，使用新的方法激活（非KMS激活），仅支持Windows10及以上版本
|[WizTree](https://www.diskanalyzer.com/)|目录占用分析，以可视化块状的方式展示哪些文件和文件夹使用的磁盘空间
|[Microsoft-Activation-Scripts](https://github.com/massgravel/Microsoft-Activation-Scripts)|Windows系统，office激活脚本，小巧，开源
|[space sniffee](http://www.uderzo.it/main_products/space_sniffer/)|目录占用分析
|[Geek uninstall](https://geekuninstaller.com/)|强大的卸载工具，轻巧，能够卸载残留
|[iobit uninstaller](http://www.iobit.com/en/update/uninstaller/)|强大的卸载工具
|[wise disk clear](https://www.wisecleaner.com/wise-disk-cleaner.html)|强大的垃圾清理工具
|[AutumnBox](https://github.com/zsh2401/AutumnBox)|图形化adb工具
|[QEFIEntryManager](https://github.com/Inokinoki/QEFIEntryManager)|efi启动项管理器
|[ViVe](https://github.com/thebookisclosed/ViVe)|Windows功能调试工具
|[HEU_KMS_Activator](https://github.com/zbezj/HEU_KMS_Activator)|激活工具
|[Bootice](http://wuyou.net/forum.php?mod=viewthread&tid=57675)|引导编辑工具


|美化||
|---|---
|[Wallpaper Engine](https://www.wallpaperengine.io/zh-hans)|视频壁纸
|[枫の美化工具箱](https://winmoes.com/tools/12948.html)|windows美化工具
|[Droptopfour](https://www.droptopfour.com/)|macos风格任务栏
|[Jaxcore](https://jaxcore.app/)|小组件工具
|[Rainmeter](https://www.rainmeter.net/)|自定义桌面工具
|[人工桌面](https://n0va.mihoyo.com/#/)|米哈游自家的视频壁纸
|[Startallback](https://startallback.cn/)|开始菜单修改
|[MicaForEveryone](https://github.com/MicaForEveryone/MicaForEveryone)|边框毛玻璃效果
|[TranslucentTB](https://github.com/TranslucentTB/TranslucentTB)|任务栏透明
|[ExplorerBlurMica](https://github.com/Maplespe/ExplorerBlurMica)|文件管理器背景透明
|[TaskbarXI](https://github.com/ChrisAnd1998/TaskbarXI)|docker栏
|[TaskbarX](https://chrisandriessen.nl/taskbarx)|docker栏
|[Aerial](https://chrisandriessen.nl/aerial)|窗口最大化的大小限制工具，配合docker栏工具使用
|[RoundedTB](https://github.com/Erisa/RoundedTB)|任务栏分段
|[lively](https://github.com/rocksdanister/lively)|动态壁纸工具


|虚拟机||
|---|---
|[VMware Workstation Pro](https://www.vmware.com/cn/products/workstation-pro/workstation-pro-evaluation.html)|功能较多，性能较强
|[VirtualBox](https://www.virtualbox.org/)|性能较弱
|[unraid](https://unraid.net/zh)|虚拟化工具，性能强
|[Proxmox](https://www.proxmox.com/en/)|开源的企业级服务器虚拟化工具，性能强
|[qemu](https://www.qemu.org/)|开源的虚拟机


|优化工具||
|---|---
|[Windhawk](https://windhawk.net/)|自定义windows框架
|[Autohotkey](https://www.autohotkey.com/)|快捷键设置
|[DesktopOK](http://www.softwareok.com/?seite=Freeware/DesktopOK)|桌面图标位置备份
|[ContextMenuManager](https://github.com/BluePointLilac/ContextMenuManager)|右键菜单管理
|[MyComputerManager](https://github.com/1357310795/MyComputerManager)|文件管理器快捷图标管理
|[Protect-Windows-Context-Menu](https://github.com/HaujetZhao/Protect-Windows-Context-Menu)|右键菜单保护工具
|[Windows 11 Classic Context Menu](https://www.sordum.org/14479/windows-11-classic-context-menu-v1-1/)|windows经典右键菜单还原
|[ContextMenuForWindows11](https://github.com/ikas-mc/ContextMenuForWindows11)|自定义新版右键菜单


|下载工具||
|---|---
|[Aira2](http://aria2.github.io/)|强大的下载工具
|[Office Tool Plus](https://otp.landian.vip/zh-cn/)|office下载工具
|[唧唧Down](http://client.jijidown.com/)|哔哩哔哩视频下载工具
|[AriaNg](https://github.com/mayswind/AriaNg)|图形化aria2工具
|[lux](https://github.com/iawia002/lux)|视频下载工具
|[youtube-dl](https://github.com/ytdl-org/youtube-dl)|youtube视频下载工具
|[you-get](https://github.com/soimort/you-get)|视频下载工具
|[scoop](https://github.com/ScoopInstaller/Scoop)|Windows命令行包管理器


|其他工具||
|---|---
|[火绒安全软件](https://www.huorong.cn/)|杀毒软件，能让windows defender安静下来
|[Traffic monitor](https://github.com/zhongyang219/TrafficMonitor)|网速和硬件监测工具
|[华为应用市场](https://uowap.hicloud.com/appdl-uomp-cn/campaignpreview/b96271d7-fc63-4477-889d-26e121259d9e/index.html)|华为自家应用市场
|[Bandizip](https://www.bandisoft.com/bandizip/)|强大的解压缩工具
|[7zip](https://7-zip.org/)|开源的解压缩工具
|[VLC](https://www.videolan.org/)|开源的视频播放器
|[迅飞输入法](https://srf.xunfei.cn/#/)|简洁，语音识别准确度高
|[wiseclear](https://www.wisecleaner.com/products.html)|wiseclear系列全家桶
|[gnirehtet](https://github.com/Genymobile/gnirehtet)|android有线上网工具
|[alist](https://github.com/alist-org/alist)|一个支持多存储的文件列表/WebDAV程序
|[localsend](https://github.com/localsend/localsend)|多平台文件互传工具


|网盘||
|---|---
|[夸克网盘](https://pan.quark.cn/)|不限速
|[百度网盘](https://pan.baidu.com/download#win)|“100kb/s,但空间大”
|[坚果云](https://www.jianguoyun.com/s/downloads)|支持协作
|[123云盘](https://www.123pan.com/Downloadclient)|空间大，不限速
|[Onedrive](https://www.microsoft.com/zh-cn/microsoft-365/onedrive/)|microsoft官方网盘
|[阿里云盘](https://www.aliyundrive.com/download)|不限速


|动捕||
|---|---
|[AI视频动捕](https://www.yunboai.com/live/)|支持3d动捕
|[vtuber studio](https://www.denchisoft.com/)|支持2d动捕
|[PrprLive](https://store.steampowered.com/app/1279610/PrprLive/?l=tchinese)|支持2d动捕，免费


|自建网盘||
|---|---
|[WinNAS](https://tilldream.com/portal.php?mod=view&aid=95)|windows下的nas搭建工具
|[Cloudreve](https://cloudreve.org/)|部署公私兼备的网盘系统
|[Openstack](https://docs.openstack.org/zh_CN/)|nas系统
|[truenas](https://www.truenas.com/)|nas系统



|我的世界工具||
|---|---
|[Minecraft Server bedrock](https://www.minecraft.net/en-us/download/server/bedrock)|官方基岩版开服器
|[Minecraft Server Java](https://www.minecraft.net/zh-hans/download/server)|官方java版开服器
|[PCL2](https://afdian.net/p/0164034c016c11ebafcb52540025c377)|第三方启动器
|[HMCL](https://hmcl.huangyuhui.net/)|第三方启动器，支持多平台
|[AMULET](https://www.amuletmc.com/)|地图编辑器
|[chunker](https://chunker.app/)|地图编辑器
|[MCC Tool Chest PE](https://www.kirimasharo.com/archives/MCC_Tool_Chest_PE.html)|地图编辑器
|[Minecraft for Windows](https://www.xbox.com/zh-CN/games/store/minecraft-for-windows/9nblggh2jhxj?ocid=pdpshare)|我的世界基岩版


|娱乐||
|---|---
|[Steam](https://store.steampowered.com/about/)|游戏发售平台
|[MuMu模拟器](https://mumu.163.com/)|安卓模拟器


|社交||
|---|---
|[QQ](https://im.qq.com/pcqq)|即时通讯工具
|[微信](https://weixin.qq.com/)|即时通讯工具
|[Discord](https://discord.com/)|综合性社交媒体平台
|[Telegram](https://telegram.org/)|即时通讯工具
|[KOOK](https://www.kookapp.cn/)|语音沟通工具


|音乐||
|---|---
|[网易云音乐](https://music.163.com/#/download)|音乐推荐较精确
|[QQ音乐](https://y.qq.com/download/index.html)|版权多
|[酷狗音乐](https://download.kugou.com/)|中规中矩
|[iTunes](https://www.apple.com.cn/itunes/)|版权多
|[龙卷风收音机](https://cradio.live/index.htm)|收听全球任意电台
