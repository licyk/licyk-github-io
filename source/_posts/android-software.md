---
title: Android软件合集
date: 2023-07-14 16:42:57
tags:
 - android
categories:
 - 软件
---
Android软件存档
<!-- more -->
****
|玩机||
|---|---
|[Scene](http://vtools.omarea.com/)|性能调节
|[Thanox](https://tornaco.github.io/Thanox/)|Android系统管理工具
|[Shizuku](https://shizuku.rikka.app/)|为其他软件提供ADB权限
|[App Ops](https://www.coolapk.com/apk/rikka.appops)|调整各个应用的权限设置
|[InstallerX](https://www.coolapk.com/apk/com.rosan.installer.x)|第三方安装器
|[安装狮](https://github.com/dadaewq/Install-Lion)|第三方安装器
|[屏幕滤镜（PWM防闪烁）](https://www.coolapk.com/apk/com.omarea.filter)|oled屏幕防闪烁
|[冰箱 IceBox](https://www.coolapk.com/apk/com.catchingnow.icebox)|冻结软件
|[AccA](https://github.com/MatteCarra/AccA/)|智能调节充电
|[Magisk](https://github.com/topjohnwu/Magisk)|magisk管理器，支持插件的插件较多
|[KernelSU](https://github.com/tiann/KernelSU)|KernelSU管理器，新的root选择
|[KernelAdiutor](https://github.com/NHellFire/KernelAdiutor)|管理内核参数 
|[清浊](https://www.dircleaner.com/)|简洁实用的清理工具
|[gnirehtet](https://github.com/Genymobile/gnirehtet)|android有线上网工具
|[moligeek](https://github.com/yourmoln/moligeek)|web集成工具
|[uperf](https://github.com/yc9559/uperf)|Android用户态性能控制器


|监测工具||
|---|---
|[Accubattery](https://accubatteryapp.com/)|电池监测
|[Cpu Float](https://play.google.com/store/apps/details?id=com.waterdaaan.cpufloat)|cpu监测
|[Cpu Z](https://www.cpuid.com/softwares/cpu-z.html)|cpu查询和监测
|[Devcheck](https://elementalx.org/devcheck/)|硬件参数查询
|[Libchecker](https://www.coolapk.com/apk/com.absinthe.libchecker)|查看并分析 App 使用的第三方库
|[Powerful Monitor](https://play.google.com/store/apps/details?id=com.glgjing.marvel)|系统监测
|[AIDA64](https://www.aida64.com/aida64-android)|硬件监测
|[speedtest-android](https://github.com/librespeed/speedtest-android)|开源的网络测速工具


|开发工具||
|---|---
|[AidLux](https://aidlux.com/)|应用开发和部署平台
|[Termux](https://termux.dev/)|Android终端仿真器和Linux环境应用
|[Auto.js](https://pro.autojs.org/)|JavaScript IDE
|[nvm](https://github.com/nvm-sh/nvm)|node.js版本管理器


|软件外设||
|---|---
|[Audiorelay](https://audiorelay.net/)|音频串流
|[Droidcam](https://droidcam.org/)|麦克风和摄像头串流
|[Soundwire](https://georgielabs.net/)|音频串流
|[Womic](https://wolicheng.com/womic/)|麦克风串流
|[IP摄像头](https://play.google.com/store/apps/details?id=com.shenyaocn.android.WebCam)|摄像头串流
|[Irium](https://iriun.com/)|摄像头串流
|[蓝牙遥控](https://www.coolapk.com/apk/284649)|模拟蓝牙键盘


|办公||
|---|---
|[Onlyoffice](https://www.onlyoffice.com/office-for-android.aspx)|开源office工具
|[Microsoft Office](https://www.microsoft.com/zh-cn/microsoft-365/mobile/microsoft-365-mobile-apps-for-android)|功能强大的office工具
|[WPS](https://mo.wps.cn/pc-app/office-android.html)|国内的office工具
|[Slack](https://slack.com/intl/zh-cn/)|办公协作


|社交||
|---|---
|[Telegram](https://telegram.org/)|即时通讯工具
|[Discord](https://discord.com/)|综合性社交媒体平台
|[知乎](https://www.zhihu.com/oia/)|知识交流平台
|[米游社](https://bbs.mihoyo.com/download.html)|米哈游旗下社区
|[QQ](https://im.qq.com/immobile)|即时通讯工具
|[微信](https://weixin.qq.com/)|即时通讯工具


|音乐||
|---|---
|[网易云音乐](https://music.163.com/#/download)|音乐推荐较精确
|[QQ音乐](https://y.qq.com/download/download.html)|版权多
|[酷狗音乐](https://download.kugou.com/)|中规中矩
|[Apple Music](https://play.google.com/store/apps/details?id=com.apple.android.music)|版权多
|[龙卷风收音机](https://cradio.live/)|收听全球任意电台


|图片||
|---|---
|[Pixez](https://github.com/Notsfsssf/pixez-flutter)|第三方pixiv
|[堆糖](https://www.duitang.com/)|图片交流社区
|[Lofter](https://www.lofter.com)|图片交流社区


|视频||
|---|---
|[MX Player](https://www.mxplayer.in/)|多格式视频播放器
|[VLC](https://play.google.com/store/apps/details?id=org.videolan.vlc)|开源多格式视频播放器
|[哔哩哔哩](https://app.bilibili.com/)|视频弹幕网站
|[Macast](https://github.com/xfangfang/Macast)|多平台视频串流工具


|文件管理||
|---|---
|[ES文件管理器](http://www.estrongs.com/)|多功能文件管理
|[MT管理器](https://www.coolapk.com/apk/bin.mt.plus)|反编译功能多
|[NP Manager](https://github.com/githubXiaowangzi/NP-Manager)|MT管理器的加强版
|[ZArchiver](https://play.google.com/store/apps/details?id=ru.zdevs.zarchiver)|多格式文件解压缩


|工具||
|---|---
|[Gltools](https://gltools.app/)|游戏性能调教
|[Rotation Control](https://play.google.com/store/apps/details?id=org.crape.rotationcontrol)|屏幕旋转控制
|[Parallel Space](http://parallelspace-app.com/)|应用双开
|[Anywhere-](https://www.coolapk.com/apk/com.absinthe.anywhere_)|创建快捷方式工具
|[ADM](https://play.google.com/store/apps/details?id=com.dv.adm)|下载工具
|[自动点击器](https://www.coolapk.com/apk/com.zidongdianji)|宏
|[Open2share](https://github.com/linesoft2/open2share)|跨软件分享
|[快捷方式](https://www.coolapk.com/apk/com.syyf.quickpay)|创建快捷方式工具
|[FV悬浮球](http://www.fooview.com/fv-cn/)|强大的多功能悬浮窗
|[一个木涵](https://www.woobx.cn/)|小工具合集
|[Every Proxy](https://play.google.com/store/apps/details?id=com.gorillasoftware.everyproxy)|手机变成代理服务器的工具
|[FTP Share](https://www.coolapk.com/apk/com.github.ghmxr.ftpshare)|FTP文件分享服务器搭建
|[夜间屏幕](https://github.com/fython/Blackbulb)|夜间护眼工具
|[KSWEB](https://www.kslabs.ru/)|手机搭建网页
|[迅飞输入法](https://srf.xunfei.cn/)|语音输入准确度高
|[钛备份](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup)|软件备份工具，不支持android11
|[Neo Backup](https://f-droid.org/packages/com.machiav3lli.backup/)|支持android11的软件和数据备份
|[Swift Backup](https://play.google.com/store/apps/details?id=org.swiftapps.swiftbackup)|支持android11的软件和数据备份
|[坚果云Markdown](https://www.jianguoyun.com/static/html/markdown-landing/index.html)|markdown编辑器
|[Speedtest](https://www.speedtest.net/apps)|网络测速软件
|[Microg软件合集](https://microg.org/download.html)|google框架的开源实现
|[V2rayNG](https://github.com/2dust/v2rayNG)|代理工具
|[Clash](https://github.com/Kr328/ClashForAndroid)|代理工具
|[Daedalus](https://github.com/iTXTech/Daedalus)|免root修改dns
|[思源笔记](https://b3log.org/siyuan/download.html)|强大的笔记编辑和管理软件
|[bilibilias](https://github.com/1250422131/bilibilias)|bilibili下载链接解析工具
|[fcitx5](https://github.com/fcitx5-android/fcitx5-android)|fcitx输入法
|[lux](https://github.com/iawia002/lux)|视频下载工具
|[youtube-dl](https://github.com/ytdl-org/youtube-dl)|youtube视频下载工具
|[you-get](https://github.com/soimort/you-get)|视频下载工具
|[tmoe](https://github.com/2moe/tmoe)|termux安装linux工具
|[PojavLauncher](https://github.com/PojavLauncherTeam/PojavLauncher)|我的世界java版启动器
|[localsend](https://github.com/localsend/localsend)|多平台文件互传工具


|美化||
|---|---
|[兽耳助手](https://www.mimikko.cn/)|桌面live2d展示
|[Volume Styles](https://play.google.com/store/apps/details?id=com.tombayley.volumepanel)|音量调节界面美化
|[Nova Launcher](https://novalauncher.com/)|第三方桌面


|浏览器||
|---|---
|[狐猴浏览器](https://lemurbrowser.com/app/zh/)|支持chrome和edge插件
|[via](https://viayoo.com/zh-cn/)|简洁，小巧
|[X浏览器](https://www.xbext.com/)|简洁，小巧
|[360极速浏览器](http://m.app.haosou.com/detail/index?pname=com.qihoo.contents&id=3964335)|360良心作
|[Firefox](https://play.google.com/store/apps/details?id=org.mozilla.firefox)|firefox手机版，功能有点少
|[Firefox Nightly](https://play.google.com/store/apps/details?id=org.mozilla.fenix)|支持安装插件
|[Chrome](https://play.google.com/store/apps/details?id=com.android.chrome)|chrome手机版，国内书签同步404
|[Microsoft Edge](https://play.google.com/store/apps/details?id=com.microsoft.emmx)|edge浏览器手机版，支持书签同步
|[BookmarkHub](https://github.com/dudor/BookmarkHub)|跨浏览器书签同步插件
|[PixivBatchDownloader](https://github.com/xuejianxianzun/PixivBatchDownloader)|pixiv批量下载工具


|远程控制||
|---|---
|[Newdesk](http://www.helpercow.com/)|多平台远程桌面工具
|[Spacedesk](https://www.spacedesk.net/)|电脑屏幕扩展
|[Raylink](https://www.raylink.live/)|多平台远程桌面工具
|[贝锐向日葵](https://sunlogin.oray.com/)|多平台远程桌面工具
|[Todesk](https://www.todesk.com/)|多平台远程桌面工具
|[Parsec](https://parsec.app/)|低延迟游戏串流
|[微软远程桌面](https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx&hl=en_US)|微软官方的远程桌面控制
|[Monect](https://www.monect.com/)|远程控制


|虚拟机||
|---|---
|[两仪](https://github.com/twoyi/twoyi)|手机虚拟机，已停更
|[光速虚拟机](https://gsxnj.cn/index)|某些性能方面比vmos强
|[VMOS](http://www.vmos.cn/)|功能比较全
|[X8沙箱](https://www.x8ds.com/)|自带应用变速器


|应用市场||
|---|---
|[Taptap](https://www.taptap.cn/mobile)|游戏搜索平台
|[百度手机助手](https://mobile.baidu.com/item?docid=5004337511&f0=search_suggestContent%400_appBaseNormal%401)|百度旗下
|[360手机助手](http://m.app.haosou.com/detail/index?pname=com.qihoo.appstore&id=100084)|360旗下
|[应用宝](https://sj.qq.com/appdetail/com.tencent.android.qqdownloader)|腾讯旗下
|[应用汇](http://m.appchina.com/app/com.yingyonghui.market)|历史版本多
|[酷安](https://www.coolapk.com/)|玩机必备
|[豌豆荚](https://www.wandoujia.com/apps/280001)|历史版本多
|[PP助手](https://www.25pp.com/android.html?channel=PP_103)|阿里巴巴旗下
|[Apkpure](https://m.apkpure.com/cn/apkpure/com.apkpure.aegon/download/3192527-apk?utm_content=1006&icn=aegon&ici=text_home-m&from=text_home-m?icn=aegon&ici=text_home-m&utm_content=1033)|可以下载google play store的付费软件
|[Malavida](https://www.malavida.com/en/soft/malavida-app-store/android/)|国外的应用市场
|[F-Droid](https://f-droid.org/)|开源的应用市场
|[华为应用市场](https://appgallery.huawei.com/app/C27162)|华为官方
|[OPPO软件商店](https://store.oppomobile.com/)|oppo官方
|[VIVO应用商店](http://info.appstore.vivo.com.cn/detail/51699)|vivo官方


|网盘||
|---|---
|[百度网盘](https://pan.baidu.com/download)|“100kb/s”,但是空间大
|[坚果云](https://www.jianguoyun.com/)|支持团队协作
|[Onedrive](https://www.microsoft.com/zh-cn/microsoft-365/onedrive/mobile)|microsoft官方网盘
|[123云盘](https://www.123pan.com/Downloadclient)|空间大，目前不限速
|[蓝奏云](https://up.woozooo.com/)|限制文件大小，但空间无限
|[阿里云盘](https://www.aliyundrive.com/download)|速度快
