---
title: Linux软件合集
date: 2023-07-15 18:51:22
tags:
 - linux
categories:
 - 软件
---
Linux软件存档
<!-- more -->
|软件包寻找途径||
|---|---
|[flathub](https://flathub.org/)|
|[aur](https://aur.archlinux.org/)|
|[星火应用商店](https://spark-app.store/)|
|[pkgs](https://pkgs.org/)|
|[launchpad](https://launchpad.net/)|


|桌面环境||
|---|---
|[gnome](https://www.gnome.org/)|
|[gnome flashback](https://wiki.gnome.org/Projects/GnomeFlashback)|
|[xfce](https://www.xfce.org/)|
|[kde plasma](https://kde.org/plasma-desktop/)|
|[cinnamon](https://github.com/linuxmint/cinnamon-desktop)|
|[mate](https://mate-desktop.org/)|
|[lxde](http://www.lxde.org/)|
|[lxqt](https://lxqt-project.org/)|
|[unity](https://ubuntuunity.org/)|
|[dde](https://www.deepin.org/zh/dde/)|
|[cutefish](https://github.com/cutefishos)|


|系统||
|---|---
|[gparted](https://gparted.org/)|分区编辑工具
|[refind](https://www.rodsbooks.com/refind/)|uefi引导管理器
|[grub-customizer](https://github.com/mdeguzis/grub-customizer/tree/master)|图形化grub编辑器
|[lightdm-settings](https://github.com/linuxmint/lightdm-settings)|lightdm编辑器，需安装slick-greeter
|[software-properties-gtk](https://packages.debian.org/bookworm/software-properties-gtk)|软件源编辑器
|[dconf-editor](https://gitlab.gnome.org/GNOME/dconf-editor)|类似注册表编辑器
|[stacer](https://oguzhaninan.github.io/Stacer-Web/)|系统管理优化
|[hardinfo](https://github.com/lpereira/hardinfo)|硬件信息检测
|[synaptic](https://www.nongnu.org/synaptic/)|图形化包管理器
|[qt5ct](https://github.com/desktop-app/qt5ct)|QT5配置工具
|[gdebi](https://github.com/linuxmint/gdebi)|deb包安装器
|[aptitude](https://packages.debian.org/sid/aptitude)|TUI软件包管理器
|[arch-install-scripts](https://github.com/archlinux/arch-install-scripts)|系统安装脚本，包括arch-chroot，genfstab，pacstrap
|[etcher](https://www.balena.io/etcher/)|启动盘制作器
|[ventoy](https://www.ventoy.net/cn/)|多系统启动盘制作工具
|[QEFIEntryManager](https://github.com/Inokinoki/QEFIEntryManager)|efi启动项管理器
|[R-Linux](https://www.r-studio.com/zhcn/free-linux-recovery/Download.shtml)|数据恢复工具


|工具||
|---|---
|[gnome-software](https://gitlab.gnome.org/GNOME/gnome-software)|gnome应用市场
|[plasma-discover](https://invent.kde.org/plasma/discover)|kde应用市场
|[flatpak](https://flatpak.org/)|flatpak包管理器
|[nix](https://nixos.org/)|nix包管理器
|[homebrew](https://github.com/Homebrew/brew)|包管理器
|[baobab](https://gitlab.gnome.org/GNOME/baobab)|空间占用分析
|[aptik](https://github.com/teejee2008/aptik)|系统备份还原工具
|[timeshift](https://github.com/linuxmint/timeshift)|系统快照工具
|[pulseeffects](https://github.com/wwmm/easyeffects)|音频均衡器
|[vim](https://www.vim.org/download.php)|TUI文本编辑器
|[nemo](https://github.com/linuxmint/nemo)|文件管理器
|[mc](https://midnight-commander.org/)|TUI文件管理器
|[tmux](https://github.com/tmux/tmux/wiki)|终端复用工具
|[scrcpy](https://github.com/Genymobile/scrcpy)|电脑控制手机工具
|[onboard](https://launchpad.net/onboard)|屏幕键盘
|[plank](https://github.com/ricotz/plank)|dock栏
|[vlc](https://www.videolan.org/vlc/)|视频播放器
|[rhythmbox](http://www.rhythmbox.org/)|音乐播放器
|[htop](https://htop.dev/)|终端任务管理器
|[xsensors](https://github.com/Mystro256/xsensors)|硬件温度监视
|[MissionCenter](https://missioncenter.io/)|任务管理器
|[fcitx5](https://github.com/fcitx/fcitx5)|输入法框架
|[ibus](https://github.com/ibus/ibus)|输入法框架
|[carbonyl](https://github.com/fathyb/carbonyl)|终端浏览器
|[nvm](https://github.com/nvm-sh/nvm)|node.js版本管理器
|[AriaNg](https://github.com/mayswind/AriaNg)|图形化aria2工具
|[win2xcur](https://github.com/quantum5/win2xcur)|windows鼠标指针转换成linux鼠标指针格式的工具
|[ssfconv](https://github.com/fkxxyz/ssfconv)|搜狗输入法皮肤格式转换成fcitx输入法格式的工具
|[flatseal](https://github.com/tchx84/Flatseal)|flatpak软件权限管理
|[nethogs](https://github.com/raboof/nethogs)|查看进程占用带宽情况
|[iozone](http://www.iozone.org/)|硬盘读取性能测试
|[iotop](http://guichaz.free.fr/iotop/)|实时监控硬盘
|[iptraf](https://github.com/iptraf-ng/iptraf-ng)|网络流量监控
|[iftop](http://www.ex-parrot.com/~pdw/iftop/)|网络流量监控
|[nmon](https://nmon.sourceforge.io/pmwiki.php)|系统资源监控
|[multitail](https://packages.debian.org/bookworm/multitail)|多日志监控
|[fail2ban](https://github.com/fail2ban/fail2ban)|SSH 暴力破解防护
|[agedu](https://www.chiark.greenend.org.uk/~sgtatham/agedu/)|页面显示磁盘空间使用情况
|[nmap](https://nmap.org/)|网络安全扫描工具
|[httperf](https://github.com/httperf/httperf)|Web压力测试
|[MinecraftBedrockServer](https://github.com/TheRemote/MinecraftBedrockServer)|我的世界基岩版开服器
|[gnirehtet](https://github.com/Genymobile/gnirehtet)|android有线上网工具
|[BookmarkHub](https://github.com/dudor/BookmarkHub)|跨浏览器书签同步插件
|[PixivBatchDownloader](https://github.com/xuejianxianzun/PixivBatchDownloader)|pixiv批量下载工具
|[deepin-wine](https://github.com/zq1997/deepin-wine)|deepin-wine源
|[fantascene-dynamic-wallpaper](https://github.com/dependon/fantascene-dynamic-wallpaper)|动态壁纸工具
|[linux-wallpaperengine](https://github.com/Almamu/linux-wallpaperengine)|动态壁纸工具
|[hidamari](https://github.com/jeffshee/hidamari)|动态壁纸工具
|[komorebi](https://github.com/cheesecakeufo/komorebi)|动态壁纸工具
|[nemo_actions_and_cinnamon_scripts](https://github.com/smurphos/nemo_actions_and_cinnamon_scripts)|nemo工具合集
|[frp](https://github.com/fatedier/frp)|端口映射工具
|[moligeek](https://github.com/yourmoln/moligeek)|web集成工具
|[PeaZip](https://github.com/peazip/PeaZip)|图形化解压工具
|[browsh](https://github.com/browsh-org/browsh)|终端浏览器
|[localsend](https://github.com/localsend/localsend)|多平台文件互传工具


|wine||
|---|---
|[winehq](https://www.winehq.org/)|windows兼容层
|[lutris](https://lutris.net/)|基于wine的游戏安装工具
|[playonlinux](https://www.playonlinux.com/en/)|基于wine的配置工具
|[winetricks](https://wiki.winehq.org/Winetricks)|wine配置工具
|[dxvk](https://github.com/doitsujin/dxvk)|vulkan加速库
|[Proton](https://github.com/ValveSoftware/Proton)|专为游戏优化的wine


|安卓容器||
|---|---
|[anbox](https://github.com/anbox)|安卓容器
|[waydroid](https://waydro.id/)|安卓容器，性能比anbox强；需要在wayland下运行，在xorg下需通过weston运行
|[redroid](https://github.com/remote-android/redroid-doc)|使用docker运行
|[waydroid_script](https://github.com/casualsnek/waydroid_script)|waydroid工具

|虚拟机||
|---|---
|virt-viewer virt-manager libvirt-clients virt-manager bridge-utils qemu qemu-kvm ovmf|kvm/qemu虚拟机软件包
|[virtualbox](https://www.virtualbox.org/)|开源的虚拟机
|[qemu](https://www.qemu.org/)|开源的虚拟机
|[quickgui](https://github.com/quickemu-project/quickgui)|基于Flutter的虚拟机前端
|[OSX-KVM](https://github.com/kholia/OSX-KVM)|在kvm虚拟机上运行MacOS
|[Docker-OSX](https://github.com/sickcodes/Docker-OSX)|在docker上运行MacOS


|下载工具||
|---|---
|[uget](https://ugetdm.com/)|图形化下载器
|[aria2](https://aria2.github.io/)|命令行下载工具
|[curl](https://curl.se/)|命令行下载工具
|[wget](https://www.gnu.org/software/wget/)|命令行下载工具
|[transmission-gtk](https://transmissionbt.com/)|BT下载器
|[lux](https://github.com/iawia002/lux)|视频下载工具
|[youtube-dl](https://github.com/ytdl-org/youtube-dl)|youtube视频下载工具
|[you-get](https://github.com/soimort/you-get)|视频下载工具


|远程桌面||
|---|---
|[newdesk](http://www.helpercow.com/)|多平台远程桌面工具
|[贝锐向日葵](https://sunlogin.oray.com/)|多平台远程桌面工具
|[rustdesk](https://rustdesk.com/zh/)|可自建的开源远程桌面工具
|[todesk](https://www.todesk.com/)|多平台远程桌面工具
|[Parsec](https://parsec.app/)|低延迟游戏串流
|[remmina](https://remmina.org/)|远程桌面连接


|串流||
|---|---
|[DroidCam](https://www.dev47apps.com/)|摄像头和麦克风串流
|[audiorelay](https://audiorelay.net/)|音频串流
|[SoundWire](https://georgielabs.net/)|音频串流
|[Webcam](http://iriun.com/)|摄像头串流
|[Macast](https://github.com/xfangfang/Macast)|多平台视频串流工具


|生产||
|---|---
|[onlyoffice](https://www.onlyoffice.com/zh/)|开源office工具
|[libreoffice](https://www.libreoffice.org/)|文档办公工具
|[handbrake](https://handbrake.fr/)|视频压制
|[krita](https://krita.org/zh/)|绘画工具
|[inkscape](https://inkscape.org/)|矢量图制作工具
|[obs-studio](https://obsproject.com/)|屏幕录像
|[Davinci Resolve](https://www.blackmagicdesign.com/cn/products/davinciresolve/)|视频编辑
|[blender](https://www.blender.org/)|3D建模
|[gimp](https://www.gimp.org/downloads/)|图片编辑
|[kdenlive](https://kdenlive.org/zh/features-zh/)|视频编辑
|[thunderbird](https://www.thunderbird.net/zh-CN/)|邮件管理
|[slack](https://slack.com/intl/zh-cn/downloads/windows)|办公协作
|[思源笔记](https://b3log.org/siyuan/download.html)|强大的笔记编辑和管理软件
|[Audacity](https://www.audacityteam.org/)|音频处理
|[Grasscutter](https://github.com/Grasscutters/Grasscutter)|游戏后端服务器


|代理工具||
|---|---
|[clash](https://github.com/Fndroid/clash_for_windows_pkg)|代理工具
|[v2ray](https://www.v2ray.com/chapter_00/install.html)|代理内核
|[zerotier](https://www.zerotier.com/)|内网穿透工具
|[Watt Toolkit](https://steampp.net/)|代理工具
|[v2raya](https://github.com/v2rayA/v2rayA)|代理工具
|[warp](https://1.1.1.1/)|代理工具


|社交||
|---|---
|[Discord](https://discord.com/)|综合性社交媒体平台
|[telegram](https://telegram.org/)|即时通讯工具
|[QQ](https://im.qq.com/)|即时通讯工具


|开发||
|---|---
|[visual studio code](https://code.visualstudio.com/)|强大的代码编辑器
|[jetbrains](https://www.jetbrains.com/)|集成式开发环境


|php||
|---|---
|[typecho](https://github.com/typecho/typecho)|博客程序
|[imgurl](https://github.com/helloxz/imgurl)|图床平台
|[speedtest](https://github.com/librespeed/speedtest)|网络测速平台


|其他||
|---|---
|[HMCL](https://github.com/huanghongxun/HMCL)|我的世界java版启动器
|[mcpelauncher-manifest](https://github.com/ChristopherHX/mcpelauncher-manifest)|我的世界基岩版启动器
|[xanmod-linux](https://github.com/xanmod/linux)|linux内核性能优化分支
|[alist](https://github.com/alist-org/alist)|一个支持多存储的文件列表/WebDAV程序